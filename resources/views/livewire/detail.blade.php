<div>
    {{-- dd($cart['products'][0]->nama) --}}
    @if(count($cart['products']) > 0)
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        <table class="table table-borderless">
            <tbody>
                @foreach($cart['products'] as $product)
                    <tr>
                        <td>{{ $product['nama'] }}</td>
                        <td>{{ $product['amount'] == 1 ? '' : 'x' . $product['amount'] }}</td>
                        <td>Rp. {{ number_format($product['harga'], 0, ',', '.') }}</td>
                    </tr>
                @endforeach
                    <tr>
                        <td>Total</td>
                        <td></td>
                        <td>Rp. {{ number_format($total, 0, ',', '.') }}</td>
                    </tr>
            </tbody>
        </table>

        <div>
            <hr>
            <h4 class="text-center text-muted" 
                style="cursor: pointer;"
                wire:click="resetInput()">
                Clear Sale
            </h4>
            <hr>
        </div>
        <div class="bill">
            <h5 style="cursor: pointer;" wire:click="save">Save Bill</h5>
            <h5 style="cursor: pointer;" wire:click="print({{ json_encode($cart['products'], TRUE) }}, '{{ $total }}')">Print Bill</h5>
        </div>
        <div class="d-grid gap-2">
            <button class="btn btn-primary" 
                type="button">
                Charge Rp. {{ number_format($total, 0, ',', '.') }}
            </button>
        </div>
    @else
        <div class="text-center w-full border-collapse p-6">
            <span class="text-lg">Tidak Ada Pesanan</span>
        </div>
    @endif
</div>
