<div style="height: 100%; cursor: pointer;" wire:click="addToCart({{ $identifier }})">
    <div class="card m-0" style="height: 100%">
        <div class="card-body">
            <img src="{{ asset('assets/img/' . $gambar) }}" class="card-img-top" alt="Cumi Tepung">
        </div>
        <div class="card-footer">
            <p class="card-text">{{ $nama }}</p>
        </div>
    </div>
</div>