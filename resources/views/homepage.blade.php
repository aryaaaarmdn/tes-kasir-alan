<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple POS APP</title>
    <link rel="stylesheet" href="{{ asset('assets/bs5/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    @livewireStyles
</head>
<body>
    <div class="container bg-light pt-4 pb-4">
        <div class="row">
            <div class="col-md-7 col-sm-12 product">
                <div class="row row row-cols-1 row-cols-md-3 g-4">
                    @foreach($data as $product)
                        <div class="col">
                            @livewire('products', ['nama' => $product->nama, 'gambar' => $product->gambar, 'harga' => $product->harga, 'identifier' => $product->id])
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-5 col-sm-12 detail">
                <div class="card">
                    <div class="card-header">
                        <img src="{{ asset('assets/img/customer.png') }}" class="avatar">
                        <h4 contenteditable="true">New Customer</h4>
                        <img src="{{ asset('assets/img/billinglist.png') }}" class="detail">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title text-center">Dine in</h5>
                        <hr>
                        @livewire('detail')
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{ asset('assets/bs5/js/bootstrap.min.js') }}"></script>
    @livewireScripts
    <script>
        Livewire.on('printing', (bill, total) => {

            let data = '';
            for( let property in bill ) {
                data = `<div style="border:1px solid black">`;

                for( let props in bill[property] ) {
                    if (props == 'nama') {
                        data += `
                            <h4>Nama</h4>
                            <p>${bill[property][props]}</p>
                        `;
                    } else if( props == 'harga') {
                        data += `
                            <h4>Harga</h4>
                            <p>${bill[property][props]}</p>
                        `;
                    } else if ( props == 'amount' ) {
                        data += `
                            <h4>Qty</h4>
                            <p>${bill[property][props]}</p>
                        `;
                    }
                }

                data += `</div>`

            }
            console.log(data);
            document.write(data);
            window.print();

        })
    </script>
</body>
</html>