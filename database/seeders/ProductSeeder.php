<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['nama' => 'Cumi Tepung', 'gambar' => 'cumitepung.jpg', 'harga' => 20000],
            ['nama' => 'Cumi Bakar', 'gambar' => 'cumibakar.jpg', 'harga' => 15000],
            ['nama' => 'Cumi Cabe', 'gambar' => 'cumicabe.jpg', 'harga' => 18000],
            ['nama' => 'Cah Kailan', 'gambar' => 'cahkailan.jpeg', 'harga' => 25000],
            ['nama' => 'Tahu Goreng', 'gambar' => 'tahugoreng.jpg', 'harga' => 10000],
            ['nama' => 'Cah Taoge', 'gambar' => 'cahtaoge.jpeg', 'harga' => 15000],
            ['nama' => 'Kerang Bambu', 'gambar' => 'kerangbambu.jpeg', 'harga' => 10000],
            ['nama' => 'Kepiting Padang', 'gambar' => 'kepitingpadang.jpeg', 'harga' => 30000],
            ['nama' => 'Gurame Asam Manis', 'gambar' => 'gurame.jpeg', 'harga' => 27000],
            ['nama' => 'Udang Bakar', 'gambar' => 'udangbakar.jpeg', 'harga' => 25000],
            ['nama' => 'Udang Saos', 'gambar' => 'udangsaos.jpeg', 'harga' => 23000],
            ['nama' => 'Udang Mayo', 'gambar' => 'udangmayo.jpeg', 'harga' => 24000],
            ['nama' => 'Iced Tea', 'gambar' => 'icedtea.jpeg', 'harga' => 7000],
            ['nama' => 'Es Jeruk Nipis', 'gambar' => 'esjeruknipis.jpeg', 'harga' => 6000],
            ['nama' => 'Granita', 'gambar' => 'granita.jpeg', 'harga' => 10000],
        ];

        foreach ($data as $data) {
            Product::create($data);
        }

    }
}
