<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Product;
use App\Facades\Cart;

class Products extends Component
{

    public $identifier;
    public $nama;
    public $gambar;

    public function render()
    {
        return view('livewire.products', ['products' => Product::all()]);
    }

    public function addToCart(int $productId)
    {
        Cart::add(Product::where('id', $productId)->first());
        $this->emit('productAdded');
    }

    
}
