<?php 

namespace App\Http\Livewire;

use Livewire\Component;
use App\Facades\Cart as CartFacade;
use Illuminate\Support\Arr;

class Detail extends Component
{

    public $cart;
    public $total;

    protected $listeners = [
        'productAdded' => 'updateCartTotal',
        'productRemoved' => 'updateCartTotal',
        'clearCart' => 'updateCartTotal'
    ];

    public function mount()
    {
        $this->cart = CartFacade::get();
        $this->total = CartFacade::charge();
    }

    public function removeFromCart($productId)
    {
        CartFacade::remove($productId);
        $this->cart = CartFacade::get();
        $this->emitSelf('productRemoved');
    }

    public function updateCartTotal()
    {
        $this->total = CartFacade::charge();
        $this->cart = CartFacade::get();
    }

    public function render()
    {
        return view('livewire.detail');
    }

    public function resetInput()
    {
        CartFacade::clear();
        $this->total = null;
        $this->emit('clearCart');
        $this->cart = CartFacade::get();
    }

    public function save()
    {
        session()->flash('message', 'Save Bill Successfully.');
        $this->cart = CartFacade::get();
    }

    public function print($cartItems, $total)
    {
        $this->emit('printing', $cartItems, $total);
    }

}
